import unittest

from persona import Persona

class Empleado(Persona):
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s, d = 0):
        super().__init__(n, d)
        if s > 0:
            self.nomina = s
        else:
            self.nomina = 0
    def calcula_impuestos (self):
        return self.nomina*0.30
    def __str__ (self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuestos())
