import unittest
from persona import Persona
from empleado import Empleado
from jefe import Jefe

class TestPersona(unittest.TestCase):
    def test_construir(self):
        e1 = Persona("nombre","02304351Y")
        self.assertEqual(e1.nif, "02304351Y")
    def test_str(self):
        e1 = Persona("pepe","02304351Y")
        self.assertEqual("La persona pepe tiene nif 02304351Y", e1.__str__())

class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)
    def test_nominaneg(self):
        e1 = Empleado("nombre",-5000)
        self.assertEqual(e1.nomina,0)
    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calcula_impuestos(), 1500)
    def test_str(self):
        e1 = Empleado("pepe",50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

class TestJefes(unittest.TestCase):
    def test_construir(self):
        e1 = Jefe("nombre",5000, 200, 1234)
        self.assertEqual(e1.nomina, 5000, 200)
    def test_nominaneg(self):
        e1 = Jefe("nombre",-5000, 200, 1234)
        self.assertEqual(e1.nomina,0)
    def test_impuestos1(self):
        e1 = Jefe("nombre",5000, 200)
        self.assertEqual(e1.calcula_impuestos1(), 1560)
    def test_impuestos(self):
        e1 = Jefe("nombre",5000)
        self.assertEqual(e1.calcula_impuestos(), 1500)
    def test_str(self):
        e1 = Jefe("pepe",50000)
        self.assertEqual("El jefe pepe debe pagar 15000.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()