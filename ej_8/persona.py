import unittest

class Persona:
    def __init__(self, n, d = 0):
        self.nombre = n
        self.nif = d
    def __str__ (self):
        return "La persona {name} tiene nif {nif}".format(name=self.nombre, nif=self.nif)