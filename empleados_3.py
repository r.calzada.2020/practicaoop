
import unittest
class Persona:
    def __init__(self, n, d):
        self.nombre = n
        self.nif = d
    def __str__ (self):
        return "La persona {name} tiene nif {nif}".format(name=self.nombre, nif=self.nif)
        
class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s
        if self.nomina < 0:
            self.nomina = 0
    def calcula_impuestos (self):
        return self.nomina*0.30
    def __str__ (self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calcula_impuestos())

class Jefe(Empleado):
    def __init__(self, n, s, extra= 0):
        super().__init__(n, s)
        self.bonus = extra
    def calcula_impuestos1 (self):
        return (self.nomina + self.bonus)*0.30

    def calcula_impuestos (self):
        return super().calcula_impuestos() + self.bonus*0.30

    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre, tax= self.calcula_impuestos())

class TestPersona(unittest.TestCase):
    def test_construir(self):
        e1 = Persona("nombre","02304351Y")
        self.assertEqual(e1.nif, "02304351Y")
    def test_str(self):
        e1 = Persona("pepe","02304351Y")
        self.assertEqual("La persona pepe tiene nif 02304351Y", e1.__str__())

class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)
    def test_nominaneg(self):
        e1 = Empleado("nombre",-5000)
        self.assertEqual(e1.nomina,0)
    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calcula_impuestos(), 1500)
    def test_str(self):
        e1 = Empleado("pepe",50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

class TestJefes(unittest.TestCase):
    def test_construir(self):
        e1 = Jefe("nombre",5000)
        self.assertEqual(e1.nomina, 5000)
    def test_nominaneg(self):
        e1 = Jefe("nombre",-5000)
        self.assertEqual(e1.nomina,0)
    def test_impuestos1(self):
        e1 = Jefe("nombre",5000)
        self.assertEqual(e1.calcula_impuestos1(), 1500)
    def test_impuestos(self):
        e1 = Jefe("nombre",5000)
        self.assertEqual(e1.calcula_impuestos(), 1500)
    def test_str(self):
        e1 = Jefe("pepe",50000)
        self.assertEqual("El jefe pepe debe pagar 15000.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()