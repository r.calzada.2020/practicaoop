import unittest
from empleado import Empleado

class Jefe(Empleado):
    def __init__(self, n, s, extra= 0, d = 0):
        super().__init__(n, s)
        self.bonus = extra
    def calcula_impuestos1 (self):
        return (self.nomina + self.bonus)*0.30

    def calcula_impuestos (self):
        return super().calcula_impuestos() + self.bonus*0.30

    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre, tax= self.calcula_impuestos())
